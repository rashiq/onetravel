import requests
import json


url = 'http://www.goeuro.com/GoEuroAPI/rest/api/v2/position/suggest/en/'

headers = {
    'Accept': 'application/vnd.goeuro.position-list+json',
    'User-Agent': 'Dalvik/2.0.0 (Linux; U; Android 4.4.4; Google Nexus 5 - 4.4.4 - API 19 - 1080x1920 Build/KTU84P)',
    'Host': 'www.goeuro.com',
    'Connection': 'Keep-Alive',
    'Accept-Encoding': 'gzip'
}

def search_city(word):
    r = requests.get(url+word, headers=headers)
    data =  json.loads(r.text)
    data = [{'city': c['primaryName']} for c in data]
    return data
    # print r.text

def search_city_exact(city):
    r = requests.get(url+city, headers=headers)
    data =  json.loads(r.text)[0]
    return data

# print search_city_exact('Hamburg')