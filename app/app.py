from flask import Flask, abort, request, url_for, Response
import json
from city import search_city
from bus import search_bus
from train import search_train

app = Flask(__name__, static_url_path = "")

@app.errorhandler(500)
def server_error(error):
  return jsonify({'error': 'Server Error'}, status=500)

@app.errorhandler(400)
def bad_request(error):
  return jsonify({'error': 'Bad Request'}, status=400)

@app.errorhandler(404)
def not_found(error):
  return jsonify({'error': 'Not Found'}, status=404)

@app.route('/api/search/city/<string:city>', methods = ['GET'])
def serach_city(city):
  return jsonify(search_city(city))

@app.route('/api/search/bus/<string:from_city>/<string:to_city>', methods = ['GET'])
def serach_bus(from_city, to_city):
  return jsonify(search_bus(from_city, to_city, "2015-03-19"))
  
@app.route('/api/search/train/<string:from_city>/<string:to_city>', methods = ['GET'])
def serach_train(from_city, to_city):
  return jsonify(search_train(from_city, to_city))

def jsonify(data, status=200):
  return Response(
    json.dumps(data, sort_keys=True, indent=4, separators=(',', ': ')),
    mimetype='application/json',
    status=status
  )


if __name__ == '__main__':
  app.run(host="10.201.3.251", debug=True, port=1238) 