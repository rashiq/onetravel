import requests
import json
from dateutil.parser import parse
from datetime import datetime, timedelta

url = 'http://www.busliniensuche.de/api2/search/connections'

def search_bus(from_location, to_location, when):

    headers = {
        'Accept': 'text/event-stream',
        'Content-Type': 'application/json',
        'Accept-Language': 'de-DE',
        'Accept-Currency': 'EUR',
        'Content-Length': 170,
        'User-Agent': 'Dalvik/1.6.0 (Linux; U; Android 4.4.4; A0001 Build/KTU84Q)',
        'Host': 'www.busliniensuche.de',
        'Connection': 'Keep-Alive',
        'Accept-Encoding': 'gzip',
    }

    data = {
        "allTransports": True, 
        "allowStored": True, 
        "companyId": 0, 
        "flexibility": 0, 
        "from": from_location, 
        "passengers": 1, 
        "radius": 15000, 
        "searchMode": 0, 
        "to": to_location,
        "when": when
    }

    r = requests.post(url, headers=headers, data=json.dumps(data))
    split = r.text.split('\n')

    l_filter = ['ArrivalStop', 'DepartureStop', 'Arrival', 'Departure', 'DeepLink']

    rides = []
    for i in range(len(split)-1):
        data = split[i]
        data = json.loads(data)[0]
        if not 'ArrivalStop' in data: continue 

        if 'DeepLink' in data:
            link = data['DeepLink']
        else:
            link = ''
        leg = {
            'Price': ("%0.2f" % (data['Price'],)+u"\u20AC").replace('.', ','),
            'Arrival': int(parse(data['Arrival']).strftime('%s')),
            'Departure': int(parse(data['Departure']).strftime('%s')),
            'Company': data['Company'] if 'Company' in data else '',
            'Duration': get_time(data['Duration']/1000),
            'Reservation': link
        }
        if not len(data['Steps']):
            leg['Steps'] = [
                {
                    'ArrivalStop': data['ArrivalStop'],
                    'DepartureStop': data['DepartureStop'],
                    'Arrival': int(parse(data['Arrival']).strftime('%s')),   
                    'Departure': int(parse(data['Departure']).strftime('%s'))
                }]
            rides.append(leg)
        else:
            leg['Steps'] = []
            for i in data['Steps']:
                l = {
                    'ArrivalStop': i['ArrivalStop'],
                    'DepartureStop': i['DepartureStop'],
                    'Arrival': int(parse(data['Arrival']).strftime('%s')),   
                    'Departure': int(parse(data['Departure']).strftime('%s'))
                }
                leg['Steps'].append(l)
                rides.append(leg)

    rides = sorted(rides, key=lambda x: x['Price'])
    return rides

def get_time(s):
    sec = timedelta(seconds=s)
    d = datetime(1,1,1) + sec
    return "%d:%dh" % (d.hour, d.minute)
    # with open('test2.json', 'w') as w:
    
    #     w.write(json.dumps(rides, sort_keys=True, indent=4, separators=(',', ': ')))

if __name__ == '__main__':
    print search_bus("Paris", "Barcelona", "2015-03-19")