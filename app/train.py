from __future__ import division
import requests
import json
from dateutil.parser import parse
from city import search_city_exact
from datetime import datetime, timedelta


def get_time(s):
    sec = timedelta(seconds=s)
    d = datetime(1,1,1) + sec
    return "%d:%dh" % (d.hour, d.minute)
    # with open('test2.json', 'w') as w:
    
    #     w.write(json.dumps(rides, sort_keys=True, indent=4, separators=(',', ': ')))




def search_train(from_city, to_city):
    url = 'http://www.goeuro.com/GoEuroAPI/rest/api/v3/search'

    headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Content-Length': 596,
        'User-Agent': 'Dalvik/2.0.0 (Linux; U; Android 4.4.4; Google Nexus 5 - 4.4.4 - API 19 - 1080x1920 Build/KTU84P)',
        'Host': 'www.goeuro.com',
        'Connection': 'Keep-Alive',
        'Accept-Encoding': 'gzip'
    }

    data = {
        "arrivalPosition": search_city_exact(from_city), 
        "departurePosition": search_city_exact(to_city), 
        "currency": "EUR", 
        
        "domain": "com", 
        "locale": "en", 
        "outboundDate": "2015-03-19T00:00:00.000", 
        "passengers": [
            {
                "age": 18, 
                "rebates": []
            }
        ], 
          "resultFormat": {
            "splitRoundTrip": True
        }, 
        "searchModes": [
            "directtrain", 
            "directbus", 
            "multimode"
        ]
    }

    r = requests.post(url, headers=headers, data=json.dumps(data))

    query_id =  json.loads(r.text)['queryId']
    return get_results(query_id)

def get_results(query_id):
    url = 'http://www.goeuro.com/GoEuroAPI/rest/api/v3/search/{query_id}/directtrain/outbounds?pageNum=1'.format(query_id=query_id)

    headers = {
        'User-Agent': 'Dalvik/2.0.0 (Linux; U; Android 4.4.4; Google Nexus 5 - 4.4.4 - API 19 - 1080x1920 Build/KTU84P)',
        'Host': 'www.goeuro.com',
        'Connection': 'Keep-Alive',
        'Accept-Encoding': 'gzip'
    }

    r = requests.get(url, headers=headers)
    #print json.loads(r.text)['results']

    with open('test.json', 'w') as f:
        f.write(
            json.dumps(json.loads(r.text))
        )


    api_results = json.loads(r.text)
    #print api_results
    rides = api_results['results']
    positions = api_results['positions']
    positions_reordered = {}
    for position in positions:
        positions_reordered[position['positionId']] = position

    companies = api_results['companies']
    companies_reordered = {}
    for company in companies:
        companies_reordered[company['id']] = company


    response = []
    for ride in rides:
        journeys = ride['journeys']
        for journey in journeys:

            leg = {
                    'Price': ("%0.2f" % (journey['price']['cents']/100,)+u"\u20AC").replace('.', ','),
                    'Arrival': int(parse(journey['outbound']['arrivalDateTime']).strftime('%s')),
                    'Departure': int(parse(journey['outbound']['departureDateTime']).strftime('%s')),
                    'Reservation': '',
                    'Company': companies_reordered[journey['outbound']['companyId']]['primaryName'],
                    'Duration': get_time(journey['outbound']['durationInTicks']/1000),
                    'Steps':[{
                            "ArrivalStop": positions_reordered[journey['outbound']['arrivalPositionId']]['primaryName'],
                            "DepartureStop": positions_reordered[journey['outbound']['departurePositionId']]['primaryName']
                        }]
                }

            response.append(leg)
    return response

if __name__ == '__main__':
    print search_train('Hamburg', 'Berlin')

    