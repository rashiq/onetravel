"""
    A basic client for the Kayak mobile app's server API
"""

import sys
import json
import requests
import hashlib
import random
import re
from geopy.distance import vincenty
import csv
from collections import defaultdict
import time


def memoize(f):
    """ Memoization decorator for functions taking one or more arguments. """
    class memodict(dict):
        def __init__(self, f):
            self.f = f
        def __call__(self, *args):
            return self[args]
        def __missing__(self, key):
            ret = self[key] = self.f(*key)
            return ret
    return memodict(f)

hashlib.sha256('hashlib').hexdigest()

# Replace the following two variables with your UUID and HASH
UUID = str(random.randint(100000000000000, 999999999999999))
HASH = hashlib.sha256(UUID + 'larrylarrylarry').hexdigest()[:32].upper()

def register_android():
    headers = {
        "User-Agent": "kayakandroidphone/6.3.1",
        "Accept-Encoding": "gzip,deflate",
        "Accept-Language": "en-US",
        "Host": "www.kayak.com",
        "Connection": "Keep-Alive"
    }

    params = {
        "action": "registerandroid",
        "uuid": UUID,
        "hash": HASH,
        "model": "Google+Nexus+5+-+4.4.4+-+API+19+-+1080x1920",
        "platform": "android",
        "os": "19",
        "locale": "en_US",
        "tz": "GMT",
    }

    url = "https://www.kayak.com/k/authajax"

    r = requests.get(url, headers=headers, params=params)
    
    for line in r.text.split("\n"):
        if "sid" not in line:
            continue
        return line.split("=")[1]

sid = register_android()


def search_flight(origin, destination, date1, date2):
    #print origin, destination, date1, date2
    origin_airport = origin
    dest_airport = destination
    departure_date = date1
    departure_date2 = date2
    sid = register_android()

    payload = {
        "cabin": "e",
        "travelers": "1",
        "origin1": origin_airport,
        "nearbyO1": "false",
        "destination1": dest_airport,
        "nearbyD1": "false",
        "depart_date1": departure_date,
        "depart_time1": "a",
        "depart_date_flex1": "exact",
        "origin2": dest_airport,
        "nearbyO2": "false",
        "destination2": origin_airport,
        "nearbyD2": "false",
        "depart_date2": departure_date2,
        "depart_time2": "a",
        "depart_date_flex2": "exact",
        "_sid_": sid
    }

    r = requests.get("https://www.kayak.com/api/search/V8/flight/start", params=payload)
    print r.text
    if 'searchid' in json.loads(r.text):
        searchid = json.loads(r.text)["searchid"] 
    else:
        return

    payload = {
        "currency" : "USD",
        "searchid" : searchid,
        "c" : "2000",
        "providerData" : "true",
        "nc" : "40",
        "includeopaques" : "true",
        "showAirlineLogos" : "true",
        "_sid_" : sid
    }
    
    
    time.sleep(1)
    r = requests.get("https://www.kayak.com/api/search/V8/flight/poll/", params=payload)
    #data = json.loads(r.text)
    return r.text

print search_flight("HAM", "JFK", "03/04/15", "04/03/15")




